/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mycompany.prueba02.dao.IdentificacionJpaController;
import com.mycompany.prueba02.dao.exceptions.NonexistentEntityException;
import com.mycompany.prueba02.entity.Identificacion;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ELIAS
 */
@WebServlet(name = "IdentificacionController", urlPatterns = {"/IdentificacionController"})
public class IdentificacionController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet IdentificacionController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet IdentificacionController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        System.out.println("doPost");
        
        String accion=request.getParameter("accion");
        
        if (accion.equals("ingreso")){
        try {
            String nombre = request.getParameter("nombre");
            String rut = request.getParameter("rut");
            String email=request.getParameter("email");
            String direccion=request.getParameter("direccion");
            String telefono=request.getParameter("telefono");
            
            Identificacion identificacion = new Identificacion();
            identificacion.setNombre(nombre);
            identificacion.setRut(rut);
            identificacion.setEmail(email);
            identificacion.setDireccion(direccion);
            identificacion.setTelefono(telefono);
            
            IdentificacionJpaController dao=new IdentificacionJpaController();
            
            dao.create(identificacion);
            
            
            
        } catch (Exception ex) {
            Logger.getLogger(IdentificacionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        }
        
        else{
            IdentificacionJpaController dao=new IdentificacionJpaController();
            List<Identificacion> lista=dao.findIdentificacionEntities();
            request.setAttribute("listaIdentificacion", lista);
            request.getRequestDispatcher("lista.jsp").forward(request, response);
            
        }
        processRequest(request, response);
        
        if (accion.equals("verlista")) { 
      
             IdentificacionJpaController dao=new IdentificacionJpaController();
            List<Identificacion> lista=dao.findIdentificacionEntities();
            request.setAttribute("listaSolicitudes", lista);
            request.getRequestDispatcher("lista_sol.jsp").forward(request, response); 
             
        }
        if (accion.equals("eliminar")) {
        
            try {
                String rut= request.getParameter("seleccion");
                 IdentificacionJpaController dao=new IdentificacionJpaController();
                dao.destroy(rut);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(IdentificacionController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }  
        if (accion.equals("Consultar")) {
                String rut= request.getParameter("seleccion");
                 IdentificacionJpaController dao=new IdentificacionJpaController();
                Identificacion inscripcion = dao.findIdentificacion(rut);
                request.setAttribute("Solicitud", inscripcion);
                request.getRequestDispatcher("consulta.jsp").forward(request, response); 
            
        }    
        processRequest(request, response);
        }
   
    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
