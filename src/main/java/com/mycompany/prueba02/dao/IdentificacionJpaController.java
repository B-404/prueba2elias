/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.prueba02.dao;

import com.mycompany.prueba02.dao.exceptions.NonexistentEntityException;
import com.mycompany.prueba02.dao.exceptions.PreexistingEntityException;
import com.mycompany.prueba02.entity.Identificacion;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author ELIAS
 */
public class IdentificacionJpaController implements Serializable {

    public IdentificacionJpaController() {
        
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
    

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Identificacion identificacion) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(identificacion);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findIdentificacion(identificacion.getNombre()) != null) {
                throw new PreexistingEntityException("Identificacion " + identificacion + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Identificacion identificacion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            identificacion = em.merge(identificacion);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = identificacion.getNombre();
                if (findIdentificacion(id) == null) {
                    throw new NonexistentEntityException("The identificacion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Identificacion identificacion;
            try {
                identificacion = em.getReference(Identificacion.class, id);
                identificacion.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The identificacion with id " + id + " no longer exists.", enfe);
            }
            em.remove(identificacion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Identificacion> findIdentificacionEntities() {
        return findIdentificacionEntities(true, -1, -1);
    }

    public List<Identificacion> findIdentificacionEntities(int maxResults, int firstResult) {
        return findIdentificacionEntities(false, maxResults, firstResult);
    }

    private List<Identificacion> findIdentificacionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Identificacion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Identificacion findIdentificacion(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Identificacion.class, id);
        } finally {
            em.close();
        }
    }

    public int getIdentificacionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Identificacion> rt = cq.from(Identificacion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
